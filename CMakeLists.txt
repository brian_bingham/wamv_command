cmake_minimum_required(VERSION 2.8.3)
project(wamv_command)
find_package(catkin REQUIRED COMPONENTS
  gazebo_dev
  geographic_msgs
  std_msgs
  rospy
  xacro)

catkin_package(
  CATKIN_DEPENDS xacro gazebo_dev geographic_msgs std_msgs
  )


xacro_add_files(
  worlds/wamv_command.world.xacro
  INORDER INSTALL DESTINATION worlds
  )

install(DIRECTORY models/
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/models)
