#!/usr/bin/env python
'''
Example for the WAMV Command Challenge to illustrate finding the position of 
attackers and defenders from the gazebo model_states publication
'''

import time
import rospy
from gazebo_msgs.msg import ModelStates

class Observer:
    '''
    A simple class to hold time and subscribe
    '''
    def __init__(self):
        self.last_time = rospy.get_time()

    def modelstates_callback(self,data):
        '''
        Subscribing function
        '''
        # The topic is published rapidly (e.g., 1000 Hz) so throttle
        if (rospy.get_time() - self.last_time) < 0.1:
            return
        self.last_time = rospy.get_time()
        
        # Get all the names of defenders and attackers
        defenders = [n for n in data.name if 'defender' in n]
        attackers = [n for n in data.name if 'attacker' in n]

        # For the example, just print state
        print("---- %.3f ----"%self.last_time)
        for name in defenders+attackers:
            ii = data.name.index(name)
            print("%s: x = %.1f, y = %.1f, speed = %.1f"%(name,
                                                          data.pose[ii].position.x,
                                                          data.pose[ii].position.y,
                                                          data.twist[ii].linear.x))
        
        
# Initialize the ROS node
rospy.init_node('state_observer_ex')

# We are on simulation time, so get_time will return zero
# until it receives a clock message.  Wait unil the clock is received.
print ("Waiting for /clock")
while rospy.get_time() < 0.01:
    time.sleep(0.1)

# Instatiate the class and subscribe
observer = Observer()
rospy.Subscriber("/gazebo/model_states", ModelStates, 
      observer.modelstates_callback)

# Keep program running until ROS shuts down or we exit with cntrl-C
rospy.spin()
