#!/usr/bin/env python
import rospy
import sys
import os
import time
import subprocess
import traceback
import argparse
from threading import Lock
from math import *
import tf.transformations as tft
from random import random, randint
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.srv import SetModelState, SetModelStateRequest, DeleteModel, DeleteModelRequest, SpawnModel, SpawnModelRequest
from geometry_msgs.msg import Pose
from vrx_gazebo.msg import Task

def rand_inrange(minn,maxx):
    return minn+(random()*(maxx-minn))

class Attacker:
    def __init__(self,color,x,y, u, goal_x, goal_y):
        self.color = color
        # Add a random number for unique names.  Collisions should be okay.
        self.name = 'attacker_'+color+str(randint(0,10000))
        self.sdf_param_name = "sdf_%s"%self.color
        self.position = [x,y]
        self.goal = [goal_x, goal_y]
        self.yaw = atan2(y-goal_y, x-goal_x)+pi
        self.xdot = u*cos(self.yaw)
        self.ydot = u*sin(self.yaw)

class GameState:
    def __init__(self,demo_mode=False, center_mode=False, spawn_radius=100.0):

        # Sets how close the defender needs to get to defeat
        self.intercept_radius = 6.0

        # Taget description
        self.tgt = [158,108]
        # Sets distance threshold for attacker winning
        self.tgt_radius = 5.0

        # Target as a line between A and B
        # These match the ground station items in the world
        self.tgt_a = [158.4, 148]
        self.tgt_b = [183.25, 83.9]

        # Init
        self.attackers = []
        self.level = 0
        # Parameters for where attachers start from
        self.spawn_radius = spawn_radius
        self.colors = ['red','green','yellow']

        # Running flag
        self.running = False

        # Task message
        self.task_pub = rospy.Publisher('/vrx/task/info', Task, queue_size=10)
        self.task_msg = Task()
        self.task_msg.state = "running"
        self.task_msg.name = "wamv_command"
        self.task_msg.score = 0.0
        
        self.t0 = rospy.get_time()

        # Demo mode, automatically increments level after set time.
        self.demo_mode = demo_mode
        self.demo_t0 = rospy.get_time()
        # Amount of time between levels in demo mode
        self.demo_dt = 5.0  

        # Center mode
        self.center_mode = center_mode
        
        # Mutex lock
        self.lock = Lock()

        # Initialize set model state service
        self.state_req = SetModelStateRequest()
        self.set_state_service = rospy.ServiceProxy('/gazebo/set_model_state',
                                                SetModelState, persistent=True)
        # Ditto for delete model service
        self.delete_req = DeleteModelRequest()
        self.delete_service = rospy.ServiceProxy('/gazebo/delete_model',
                                                 DeleteModel, persistent=True)
        # Establish connection to spawn models
        self.spawn_req = SpawnModelRequest()
        self.spawn_service = rospy.ServiceProxy('/gazebo/spawn_sdf_model',
                                                SpawnModel, persistent=True)

    def gameover(self):
        '''
        Attackers reached the target
        '''
        self.running = False
        # Freeze score

        # Stop attackers
        for a in self.attackers:
          self.set_state(a,xdot=0,ydot=0)
          
        self.task_msg.state = "finished"
        rospy.loginfo("GAME OVER: %f"%self.task_msg.score)

    def remove_attacker(self,a):
        # Remove from our list
        self.attackers.remove(a)

        # Remove from gazebo
        # This seems problematic - so just move them into oblivion
        #a.delete()
        a.position[0] = 10000
        a.position[1] = 10000
        a.xdot = 0
        a.ydot = 0
        self.set_state(a)
        
    def modelstates_callback(self,data):
        # Throttle
        if (rospy.get_time() - self.t0) < 0.1:
            return
        self.t0 = rospy.get_time()
        
        if not self.running:
            return
        
        # Collect defenders
        defenders = [n for n in data.name if 'defender' in n]
        if len(defenders) < 1:
            rospy.logwarn("There are no defenders, make sure your USV name contains "
                          "'defender'")
        # Collect defender locations
        defend_x = []
        defend_y = []
        for dstr in defenders:
            ii = data.name.index(dstr)
            defend_x.append(data.pose[ii].position.x)
            defend_y.append(data.pose[ii].position.y) 
        for a in self.attackers:
            if a.name in data.name:
                ii = data.name.index(a.name)
                a.position = [data.pose[ii].position.x, data.pose[ii].position.y]
                # Check proximity to defenders
                for x, y in zip(defend_x, defend_y):
                    dd = sqrt((a.position[0]-x)**2+
                              (a.position[1]-y)**2)
                    if dd < self.intercept_radius:
                        self.task_msg.score += 1
                        rospy.loginfo("Intercepted attacker <%s>: %f"%(a.name,self.task_msg.score))
                        rospy.loginfo("d=%f, attacker (%f,%f), defender (%f,%f)"%(dd,a.position[0],a.position[1], x,y))
                        self.remove_attacker(a)
                # Check proxmitiy to target
                d = sqrt((a.position[0]-a.goal[0])**2+
                         (a.position[1]-a.goal[1])**2)
                if d < self.tgt_radius:
                    rospy.loginfo("Attackers reached target!")
                    rospy.loginfo("d=%f, attacker (%f,%f), defender (%f,%f)"%(d,a.position[0],a.position[1], self.tgt[0],self.tgt[1]))
                    self.gameover()

        # If all attackers are gone - level up
        if len(self.attackers) < 1:
            rospy.loginfo("Level up because there are no more attackers!")
            self.increment_level()
            
        # If demo mode - level up
        if self.demo_mode and ((rospy.get_time() - self.demo_t0) > self.demo_dt):
            rospy.loginfo("Demo Mode - automatically leveling up")
            self.demo_t0 = rospy.get_time()
            self.increment_level()

        self.task_pub.publish(self.task_msg)
                           
    def spawn_attacker(self, a):
        rospy.loginfo("Spawn <%s>"%a.name)
        self.spawn_req.model_name = a.name
        q = tft.quaternion_from_euler(0,0,a.yaw)
        pose = Pose()
        pose.position.x = a.position[0]
        pose.position.y = a.position[1]
        pose.orientation.x = q[0]
        pose.orientation.y = q[1]
        pose.orientation.z = q[2]
        pose.orientation.w = q[3]
        self.spawn_req.initial_pose = pose
        try:
            self.spawn_req.model_xml = rospy.get_param(a.sdf_param_name)
        except KeyError:
            rospy.logfatal('<%s> is missing on the parameter server - have you run " roslaunch wamv_command wamv_command.launch"?'%a.sdf_param_name)
            traceback.print_exc()
            sys.exit()
        resp = self.spawn_service(self.spawn_req)
        rospy.loginfo(resp.status_message)
        
    def increment_level(self):
        self.running = False
        self.lock.acquire()
        self.level += 1   
        rospy.loginfo("Level Up! New level = %d"%self.level) 
        # Delete any attackers
        for a in reversed(self.attackers):
            self.remove_attacker(a)
            
        for ii in range(self.level):
            # Generate goal/target along the line for each new attacker
            # Distance A-B
            D = sqrt( (self.tgt_a[0]-self.tgt_b[0])**2 +  (self.tgt_a[1]-self.tgt_b[1])**2 )
            # Rand distance to new target point
            d = rand_inrange(0.1*D,0.9*D)
            # Angle of line
            la = atan2(self.tgt_b[1]-self.tgt_a[1],self.tgt_b[0]-self.tgt_a[0])
            # Set the goal/target for each attacker to be along the line
            goal = [ self.tgt_a[0] + d*cos(la),
                     self.tgt_a[1] + d*sin(la) ]

            # For testing, set the goal to the initial wamv position
            if self.center_mode:
                goal =  [158,108]
            
            # Generate starting point
            offset = pi/4
            ang = rand_inrange(la-pi/2.0-offset, la-pi/2.0+offset)
            x = goal[0] + self.spawn_radius*cos(ang)
            y = goal[1] + self.spawn_radius*sin(ang)
            
            #  New attacker
            u = self.level*0.75*rand_inrange(0.5,1.5)
            if not self.demo_mode:
                u = min(u,3.0)
            a = Attacker(self.colors[ii%len(self.colors)]
                         ,x,y,u,goal[0],goal[1])
            # Spawn
            self.spawn_attacker(a)
            self.attackers.append(a)
        self.lock.release()
        # Now make them move
        for a in self.attackers:
            self.set_state(a)
        self.running = True
        # Restart demo timer
        self.demo_t0 = rospy.get_time()

    def set_state(self, attacker, x=None, y=None, xdot=None, ydot=None):
        '''
        Call gazebo servie to set state
        '''
        self.state_req.model_state.model_name = attacker.name
        rospy.loginfo("Set state for <%s>"%self.state_req.model_state.model_name)
        if xdot is None:
            xdot = attacker.xdot
        if ydot is None:
            ydot = attacker.ydot
        if x is None:
            x = attacker.position[0]
        if y is None:
            y = attacker.position[1]
        q = tft.quaternion_from_euler(0,0,attacker.yaw)
        self.state_req.model_state.pose.position.x = x
        self.state_req.model_state.pose.position.y = y
        self.state_req.model_state.pose.orientation.x = q[0]
        self.state_req.model_state.pose.orientation.y = q[1]
        self.state_req.model_state.pose.orientation.z = q[2]
        self.state_req.model_state.pose.orientation.w = q[3]
        self.state_req.model_state.twist.linear.x = xdot
        self.state_req.model_state.twist.linear.y = ydot

        resp = self.set_state_service(self.state_req)
        rospy.loginfo(resp.status_message)
        #subprocess.call("rosservice call /gazebo/set_model_state '{model_state: { model_name: %s, pose: { position: { x: %f, y: %f ,z: 0.0 }, orientation: {x: %f, y: %f, z: %f, w: %f } }, twist: { linear: {x: %f , y: %f ,z: 0 } , angular: { x: 0.0 , y: 0 , z: 0.0 } } , reference_frame: world } }'"%
        # (self.name,self.position[0],self.position[1],q[0],q[1],q[2],q[3],xdot,ydot),
        #shell=True )

def main(argv=None):
    # This enables us to invoke this function with arguments from a script
    if argv is None:
        argv = sys.argv

    # Define our arguments
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class = \
                                         argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-d','--demo_mode', action='store_true',
                        help="Demo mode where automatically increments to "
                        "level after set time")
    parser.add_argument('-c','--center_mode', action='store_true',
                        help="Mode were attackers advance toward the "
                        "initial wamv location")
    parser.add_argument('-v','--verbose', action='store_true',
                        help="Sets logging level to DEBUG")
    parser.add_argument('-s','--spawn_radius', type=float,
                        default=75.0, help="Set spawn radius. Default: %(default)s")
    
    args = parser.parse_args(argv[1:])

    level = rospy.INFO
    if args.verbose:
        level = rospy.DEBUG
    # Start node        
    rospy.init_node('wamv_command', log_level=level)
    rospy.logdebug( args.demo_mode)
    # Wait for the clock to be initiated
    rospy.logdebug("Waiting for /clock")
    while rospy.get_time() < 0.01:
        time.sleep(0.1)

    # Instatiate game object
    gs = GameState(demo_mode=args.demo_mode, center_mode=args.center_mode,
                   spawn_radius=args.spawn_radius)

    # Subscribe to gazebo output
    rospy.Subscriber("/gazebo/model_states", ModelStates, 
      gs.modelstates_callback)

    # Kickoff at level 1
    gs.increment_level()

    # Keep spinning
    rospy.spin()

if __name__ == "__main__":
    main()
