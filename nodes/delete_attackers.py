import rospy, sys, os, time
import string
from gazebo_msgs.srv import SetModelState, SetModelStateRequest, DeleteModel, DeleteModelRequest, SpawnModel, SpawnModelRequest, GetWorldProperties, GetWorldPropertiesRequest

# Call the get_world_properties service
world_req = GetWorldPropertiesRequest()
world_service = rospy.ServiceProxy('/gazebo/get_world_properties',
                                   GetWorldProperties)
print "Calling the get_world_properties service to find model names"
resp = world_service(world_req)

# Setup delete servide
delete_req = DeleteModelRequest()
delete_service = rospy.ServiceProxy('/gazebo/delete_model',
                                    DeleteModel)
    
for a in resp.model_names:
    if 'attacker' in a:
        print "Attempting to delete <%s>"%a
        delete_req.model_name = a
        resp = delete_service(delete_req)
        print resp
        
