


roslaunch wamv_command sandisland.launch verbose:=true

# Uses URDF - but can't turn off gravity
#roslaunch wamv_command wamv_attacker.launch 

#rosrun gazebo_ros spawn_model -f ./wamv_red.sdf -sdf -model test

# Uses SDF - but can't get model relative path to work.
roslaunch wamv_command spawn_attackers_sdf.launch
roslaunch wamv_command load_attackers_sdf.launch 


# test sdf
 /opt/ros/melodic/share/xacro/xacro.py ~/vrx_ws/src/wamv_command/sdf/wamv_color.sdf.xacro color:=green


rosservice call /gazebo/delete_model "model_name: 'wamv'" 
rosservice call /gazebo/delete_model "model_name: 'attacker'" 
rosservice call /gazebo/delete_model "model_name: 'attacker_red'" 
rosservice call /gazebo/delete_model "model_name: 'attacker_yellow'" 
rosservice call /gazebo/delete_model "model_name: 'attacker_green'" 


 rosservice call /gazebo/set_model_state "model_state:
  model_name: 'attacker_red'
  pose:
    position:
      x: 100
      y: 1
      z: 0.0
    orientation:
      x: 0.0
      y: 0.0
      z: 0.0
      w: 0.0
  twist:
    linear:
      x: 1.0
      y: 0.0
      z: 0.0
    angular:
      x: 0.0
      y: 0.0
      z: 0.0
  reference_frame: ''" 

rosservice call /gazebo/set_model_state '{model_state: { model_name: rrbot, pose: { position: { x: 1, y: 1 ,z: 10 }, orientation: {x: 0, y: 0.491983115673, z: 0, w: 0.870604813099 } }, twist: { linear: {x: 0.0 , y: 0 ,z: 0 } , angular: { x: 0.0 , y: 0 , z: 0.0 } } , reference_frame: world } }'